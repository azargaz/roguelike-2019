﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovementController : MonoBehaviour
{  
    public LayerMask blockingLayer;
    public float moveTime = 0.1f;
    public float jumpHeight = 0.4f;

    [Range(0f, 1f)]
    public float jumpAnimationStartingPercent = 0.5f;
    public AnimationCurve spriteMovementCurve;

    float inverseMoveTime;

    bool moving = false;

    public Transform spriteTransform;
    BoxCollider2D col;

    protected virtual void Start()
    {
        inverseMoveTime = 1f/moveTime;
        col = GetComponent<BoxCollider2D>();
    }

    protected bool Move(int xDir, int yDir, out RaycastHit2D hit)
    {
        Vector3 start = transform.position;
        Vector3 dir = new Vector3(xDir, yDir, 0);
        Vector3 end = start + dir;
        
        col.enabled = false;
        hit = Physics2D.Linecast(start, end, blockingLayer);
        col.enabled = true;
        
        if(!hit && !moving)
        {
            StartCoroutine(SmoothMovement(end));
            StartCoroutine(SpriteSmoothMovement());
            return true;
        }

        return false;
    }

    protected IEnumerator SmoothMovement(Vector3 end)
    {
        moving = true;
        float sqrRemainingDistance = (transform.position - end).sqrMagnitude;

        while(sqrRemainingDistance > float.Epsilon)
        {
            transform.position = Vector3.MoveTowards(transform.position, end, inverseMoveTime * Time.deltaTime);
            sqrRemainingDistance = (transform.position - end).sqrMagnitude;
            
            yield return new WaitForFixedUpdate();
        }
        moving = false;
    }

    protected IEnumerator SpriteSmoothMovement()
    {
        float timer = moveTime;
        float time = 0f;
        Vector3 end = new Vector3(0, jumpHeight, 0);

        while(time < timer)
        {
            if(time >= timer*0.5f)
                end = Vector3.zero;

            spriteTransform.localPosition = Vector3.Lerp(spriteTransform.localPosition, end, spriteMovementCurve.Evaluate(time/timer));
            time += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }

        // Vector3 end = new Vector3(0, jumpHeight, 0);
        // float sqrRemainingDistance = (spriteTransform.localPosition - end).sqrMagnitude;
        
        // while(sqrRemainingDistance > float.Epsilon)
        // {
        //     spriteTransform.localPosition = Vector3.MoveTowards(spriteTransform.localPosition, end, inverseMoveTime * Time.deltaTime);
        //     sqrRemainingDistance = (spriteTransform.localPosition - end).sqrMagnitude;
            
        //     yield return null;
        // }

        // end = new Vector3(0, 0, 0);
        // sqrRemainingDistance = (spriteTransform.localPosition - end).sqrMagnitude;

        // while(sqrRemainingDistance > float.Epsilon)
        // {
        //     spriteTransform.localPosition = Vector3.MoveTowards(spriteTransform.localPosition, end, inverseMoveTime * Time.deltaTime);
        //     sqrRemainingDistance = (spriteTransform.localPosition - end).sqrMagnitude;
            
        //     yield return null;
        // }
    }

    protected virtual void AttemptMove<T>(int xDir, int yDir) where T : Component
    {
        RaycastHit2D hit;

        bool canMove = Move(xDir, yDir, out hit);

        if(hit.transform == null)
            return;

        T hitComponent = hit.transform.GetComponent<T>();

        if(!canMove && hitComponent != null)
        {
            OnCantMove(hitComponent);
        }
    }

    protected abstract void OnCantMove<T>(T component) where T : Component;
}
