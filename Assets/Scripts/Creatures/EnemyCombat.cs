﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCombat : CombatController
{
    public override bool Attack<T>(T component)
    {
        base.Attack(component);
        
        PlayerCombat hitPlayer = component as PlayerCombat;
        int damage = stats.baseAttackDamage + stats.bonusAttackDamage;
        hitPlayer.DealDamage(damage);
        
        return true;
    }

    protected override void InflictStatusEffect(StatusEffect effect)
    {
        throw new System.NotImplementedException();
    }
}
