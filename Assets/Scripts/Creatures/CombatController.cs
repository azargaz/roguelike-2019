﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CombatController : MonoBehaviour
{
    [System.Serializable]
    public class Stats
    {
        public int curHealth;
        public int maxHealth = 1;
        public int curShield;
        public int maxShield = 0;

        public int baseAttackDamage = 1;
        public int bonusAttackDamage = 0;
        public int attacksPerTurn = 1;
        public int bonnusAttacksPerTurn = 0;

        public float critChance = 0f;
        public float critMultiplier = 1.5f;

        public void Initialize()
        {
            curHealth = maxHealth;
            curShield = maxShield;
        }
    }

    public enum StatusEffect { none, stunned, frozen, invincible, damageOverTime }
    [HideInInspector]
    public List<StatusEffect> currentStatusEffects = new List<StatusEffect>();

    public Stats stats;
    bool dead = false;

    Animator animator;

    protected virtual void Start()
    {
        stats.Initialize();
        animator = GetComponentInChildren<Animator>();
    }

    protected virtual void Update() 
    {
        if(stats.curHealth <= 0 && !dead)
        {
            Death();
        }
    }

    public virtual bool Attack<T>(T component) where T : Component
    {
        animator.SetTrigger("Attack");
        print("[" + gameObject.name + "] ATTACKS [" + component.name + "]");
        return true;
    }

    public virtual bool DealDamage(int damage)
    {
        stats.curHealth -= damage;
        stats.curHealth = Mathf.Clamp(stats.curHealth, 0, stats.maxHealth);
        animator.SetTrigger("Damaged");
        print("[" + gameObject.name + "] TOOK " + damage + " DAMAGE!");
        return true;
    }

    protected abstract void InflictStatusEffect(StatusEffect effect);

    public void Death()
    {
        dead = true;
        animator.SetTrigger("Death");
        print("[" + gameObject.name + "] DIED.");
    }
}
