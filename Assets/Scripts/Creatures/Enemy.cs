﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MovementController
{    
    public int skipTurns = 2;
    int turnsSkipped = 0;

    public int attackRange = 1;
    public int aggroRange = 20;
    [HideInInspector]
    public bool aggroed = false;
    public bool diagonalMovement = false;

    EnemyCombat combatController;
    [HideInInspector]
    public Transform target;

    protected override void Start()
    {   
        // Adds itself to list of enemies in turn manager
        TurnManager.instance.AddEnemy(this);

        combatController = GetComponent<EnemyCombat>();
        target = GameObject.FindGameObjectWithTag ("Player").transform;

        base.Start();
    }

    int xDir = 0;
    int yDir = 0;

    public Vector3 PlanMoveEnemy()
    {
        if(Vector3.Distance(target.transform.position, transform.position) > aggroRange)
        {
            aggroed = false;
            return Vector3.zero;
        }
        else
        {
            aggroed = true;
        }

        xDir = 0;
        yDir = 0;

        // A* Test //
        Vector3[] path = TurnManager.instance.pathfinding.GetPath(transform.position, target.position, diagonalMovement);     
        /////////////
        Vector3 targetPosition = path.Length > 0 ? path[0] : target.position;

        int xDistance = (int)(targetPosition.x - transform.position.x);
        int yDistance = (int)(targetPosition.y - transform.position.y);
        int xDistanceAbs = Mathf.Abs(xDistance);
        int yDistanceAbs = Mathf.Abs(yDistance);

        // Diagonal movement      
        if(diagonalMovement)
        {
            // First choose direction
            xDir = targetPosition.x > transform.position.x ? 1 : -1; 
            yDir = targetPosition.y > transform.position.y ? 1 : -1; 

            // Check if player is in range, 
            // if yes - set direction to players position, thus attacking him
            if((xDistanceAbs > attackRange * Mathf.Sqrt(2) - Mathf.Epsilon && yDistanceAbs > attackRange * Mathf.Sqrt(2) - Mathf.Epsilon) &&
                Vector2.Distance(targetPosition, transform.position) <= attackRange * Mathf.Sqrt(2) + Mathf.Epsilon)
            {
                xDir = xDistance;
                yDir = yDistance;                
            }
        }
        // Non-diagonal movement
        else
        {          
            // Choose direction on one of two axis
            if(xDistanceAbs > yDistanceAbs)
                xDir = targetPosition.x > transform.position.x ? 1 : -1;      
            else
                yDir = targetPosition.y > transform.position.y ? 1 : -1;    

            // Check if player is in range, 
            // if yes - set direction to players position, thus attacking him
            if((xDistanceAbs <= attackRange + Mathf.Epsilon && yDistanceAbs < Mathf.Epsilon) ||
                (yDistanceAbs <= attackRange + Mathf.Epsilon && xDistanceAbs < Mathf.Epsilon))
            {
                if(xDir != 0)
                {
                    xDir = xDistance;
                    yDir = 0;
                }
                else if(yDir != 0)
                {
                    xDir = 0;
                    yDir = yDistance;
                }
            }
        }
        
        return new Vector3(xDir, yDir, 0);
    }

    public void MoveEnemy ()
    {    
        AttemptMove <PlayerCombat> (xDir, yDir);
    }

    protected override void AttemptMove<T>(int xDir, int yDir)
    {
        // Move only every <skipTurns> turn
        if(turnsSkipped < skipTurns)
        {
            turnsSkipped++;
            return;
        }

        base.AttemptMove<T>(xDir, yDir);
        turnsSkipped = 0;
    }

    protected override void OnCantMove<T>(T component)
    {
        combatController.Attack(component);
    }

    void OnDestroy()
    {
        // Remove itself from enemy list in turn manager in order to avoid errors
        TurnManager.instance.RemoveEnemy(this);
    }
}
