﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MovementController
{
    SpriteRenderer sprite;
    PlayerCombat combatController;

    protected override void Start()
    {
        sprite = spriteTransform.GetComponent<SpriteRenderer>();
        combatController = GetComponent<PlayerCombat>();

        base.Start();
    }

    void Update()
    {
        if(!TurnManager.instance.playersTurn)
            return;

        int horizontal = (int)Input.GetAxisRaw("Horizontal");
        int vertical = (int)Input.GetAxisRaw("Vertical");
                
        if(horizontal != 0)
            vertical = 0;
                
        if(horizontal != 0 || vertical != 0)
        {            
            AttemptMove<EnemyCombat>(horizontal, vertical);
        }

        // Flipping sprite when moving in X axis
        if(horizontal != 0)
        {
            sprite.flipX = horizontal < 0;
        }
    }

    protected override void AttemptMove<T>(int xDir, int yDir)
    {
        base.AttemptMove<T>(xDir, yDir);

        RaycastHit2D hit;

        if(Move(xDir, yDir, out hit))
        {
            // TODO: if moved maybe do something, idk
        }

        TurnManager.instance.EndPlayersTurn();
    }

    protected override void OnCantMove<T>(T component)
    {
        // TODO: actually attacking enemies
        combatController.Attack(component);
    }
}
