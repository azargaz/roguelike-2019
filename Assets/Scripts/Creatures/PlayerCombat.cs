﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : CombatController
{
    public override bool Attack<T>(T component)
    {
        base.Attack(component);
        
        EnemyCombat enemy = component as EnemyCombat;
        int damage = stats.baseAttackDamage + stats.bonusAttackDamage;
        enemy.DealDamage(damage);

        return true;
    }

    protected override void InflictStatusEffect(StatusEffect effect)
    {
        throw new System.NotImplementedException();
    }
}
