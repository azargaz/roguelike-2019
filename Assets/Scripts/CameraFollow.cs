﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    GameObject player;
    Vector3 offset;

    void Start () 
    {
        player = GameObject.FindGameObjectWithTag("Player");
        transform.position = player.transform.position;
        transform.position = new Vector3(transform.position.x, transform.position.y, -10f);  
        offset = transform.position - player.transform.position;
    }

    void LateUpdate () 
    {
        transform.position = player.transform.position + offset;
    }
}
