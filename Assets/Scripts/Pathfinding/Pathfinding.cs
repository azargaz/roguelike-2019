﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour
{
    public class Node : IHeapItem<Node>
    {
        public Vector3 worldPosition;
        public int gridX;
        public int gridY;
        public bool walkable;

        public int gCost;
        public int hCost;
        public Node parent;
        int heapIndex;

        public Node(int x, int y, bool _walkable, Vector3 _worldPos)
        {
            gridX = x;
            gridY = y;
            walkable = _walkable;
            worldPosition = _worldPos;
        }

        public int fCost
        {
            get 
            {
                return gCost + hCost;
            }
        }

        public int HeapIndex
        {
            get 
            {
                return heapIndex;
            }
            set 
            {
                heapIndex = value;
            }
        }

        public int CompareTo(Node nodeToCompare)
        {
            int compare = fCost.CompareTo(nodeToCompare.fCost);
            if(compare == 0)
            {
                compare = hCost.CompareTo(nodeToCompare.hCost);
            }
            
            return -compare;
        }
    }

    public bool drawGizmos = false;

    public int gridWidth = 50;
    public int gridHeight = 50;

    int GridMaxSize
    {
        get
        {
            return gridWidth * gridHeight;
        }
    }

    public float nodeRadius = 0.5f;
    public LayerMask unwalkable;

    public Node[,] grid;    

    List<Transform> enemyPositions;
    Vector3 bottomLeft;
    
    void Awake()
    {
        enemyPositions = new List<Transform>();
        bottomLeft = Vector3.zero;
        //bottomLeft = new Vector3(-gridWidth*0.5f, -gridHeight*0.5f, 0);
        CreateGrid();
    }

    void CreateGrid()
    {
        grid = new Node[gridWidth, gridHeight];

        for(int x = 0; x < gridWidth; x++)
        {
            for(int y = 0; y < gridHeight; y++)
            {
                Vector3 worldPos = new Vector3(x,y,0) + bottomLeft;
                bool walkable = !Physics2D.CircleCast(worldPos, nodeRadius, Vector2.up, 0f, unwalkable);
                grid[x,y] = new Node(x, y, walkable, worldPos);
            }
        }
    }

    void OnDrawGizmos()
    {
        if(drawGizmos && grid != null)
        {
            for(int x = 0; x < gridWidth; x++)
            {
                for(int y = 0; y < gridHeight; y++)
                {
                    Vector3 worldPos = new Vector3(x, y, 0) + bottomLeft;
                    Node node = GetNodeFromWorldPos(worldPos);
                    Gizmos.color = node.walkable ? Color.white : Color.red;
                    Gizmos.DrawCube(worldPos, Vector3.one * 0.9f);
                }
            }
        }
    }

    Node GetNodeFromWorldPos(Vector3 worldPos)
    {
        worldPos -= bottomLeft;
        Node node = grid[(int)worldPos.x, (int)worldPos.y];
        return node;
    }

    public void UpdateEnemyPositions()
    {
        List<Enemy> enemies = TurnManager.instance.enemies;
        enemyPositions.Clear();
        for(int i = 0; i < enemies.Count; i++)
        {
            enemyPositions.Add(enemies[i].transform);
        }
    }

    bool NodeOccupiedByEnemy(Node node)
    {
        foreach(Transform enemy in enemyPositions)
        {
            if(node.worldPosition == enemy.position)
                return true;
        }
        return false;
    }

    List<Node> GetNeighbours(Node node, bool searchDiagonal)
    {
        List<Node> neighbours = new List<Node>();

        for(int x = -1; x <= 1; x++)
        {
            for(int y = -1; y <= 1; y++)
            {
                if(x == 0 && y == 0)
                    continue;
                
                if(!searchDiagonal && Mathf.Abs(x) == Mathf.Abs(y))
                    continue;

                int checkX = node.gridX + x;
                int checkY = node.gridY + y;

                if(checkX >= 0 && checkX < gridWidth && checkY >= 0 && checkY < gridHeight)
                {
                    neighbours.Add(grid[checkX, checkY]);
                }
            }
        }

        return neighbours;
    }

    public Vector3[] GetPath(Vector3 start, Vector3 end, bool diagonalPath)
    {
        Vector3[] waypoints = new Vector3[0];
        bool pathSuccess = false;

        Node startNode = GetNodeFromWorldPos(start);
        Node endNode = GetNodeFromWorldPos(end);

        if(startNode.walkable && endNode.walkable)
        {
            Heap<Node> openSet = new Heap<Node>(GridMaxSize);
            HashSet<Node> closedSet = new HashSet<Node>();
            openSet.Add(startNode);

            while(openSet.Count > 0)
            {
                Node currentNode = openSet.RemoveFirst();
                closedSet.Add(currentNode);

                if(currentNode == endNode)
                {
                    pathSuccess = true;
                    break;
                }

                foreach(Node neighbour in GetNeighbours(currentNode, diagonalPath))
                {
                    if(!neighbour.walkable || closedSet.Contains(neighbour) || NodeOccupiedByEnemy(neighbour))
                        continue;

                    int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour);
                    if(newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                    {
                        neighbour.gCost = newMovementCostToNeighbour;
                        neighbour.hCost = GetDistance(neighbour, endNode);
                        neighbour.parent = currentNode;

                        if(!openSet.Contains(neighbour))
                            openSet.Add(neighbour);
                    }
                }
            }
        }
        
        if(pathSuccess)
        {
            waypoints = RetracePath(startNode, endNode);
        }

        return waypoints;
    }

    Vector3[] RetracePath(Node startNode, Node endNode)
    {
        List<Node> path = new List<Node>();
        Node currentNode = endNode;

        while(currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        Vector3[] waypoints = ConvertPathToWaypoints(path);
        Array.Reverse(waypoints);
        return waypoints;
    }

    Vector3[] ConvertPathToWaypoints(List<Node> path)
    {
        List<Vector3> waypoints = new List<Vector3>();

        for (int i = 1; i < path.Count; i++)
        {
            waypoints.Add(path[i].worldPosition);
        }
        return waypoints.ToArray();
    }

    int GetDistance(Node nodeA, Node nodeB)
    {
        int distX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int distY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

        if(distX > distY)
            return 14 * distY + 10 * (distX - distY);
        
        return 14 * distX + 10 * (distY - distX);
    }
}
