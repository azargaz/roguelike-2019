﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardCreator : MonoBehaviour
{
    public enum TileType
    {
        Wall, Floor, Trap, Items, Enemy
    }

    [Header("Board, Rooms & Corridors")]
    public int columns = 100;
    public int rows = 100;
    public IntRange numRooms = new IntRange(15, 20);
    public IntRange roomWidth = new IntRange(3, 10);
    public IntRange roomHeight = new IntRange(3, 10);
    public IntRange corridorLength = new IntRange(6, 10);
    public GameObject[] floorTiles;
    public GameObject[] wallTiles;
    public GameObject[] outerWallTiles;
    public GameObject player;
    public GameObject exit;

    TileType[][] tiles;
    Room[] rooms;
    Corridor[] corridors;
    GameObject boardHolder;

    [Header("Traps")]
    public GameObject[] trapTiles;

    public IntRange trapsPerRoom = new IntRange(1, 6);
    public int trapSpawnChance = 50;

    // Monster spawn stuff
    [Header("Monsters")]
    public GameObject[] enemies;

    public IntRange monstersPerRoom = new IntRange(1, 3);
    public int monsterSpawnChance = 50;

    [Header("Items")]
    public GameObject[] items;

    public IntRange itemsPerLevel = new IntRange(3, 5);

    GameObject spawnedPlayer;
    GameObject spawnedExit;
    Room startingRoom;

    void Awake()
    {
        CreateBoard();
    }

    void CreateBoard()
    {
        boardHolder = new GameObject("BoardHolder");

        SetupTilesArray();

        CreateRoomsAndCorridors();

        SetTilesValuesForRooms();
        SetTilesValuesForCorridors();

        // Make sure there is always path without traps
        CreateClearPath();

        InstantiateTiles();
        InstantiateOuterWalls();
    }

    void SetupTilesArray()
    {
        tiles = new TileType[columns][];

        for (int i = 0; i < tiles.Length; i++)
        {
            tiles[i] = new TileType[rows];
        }
    }

    void CreateRoomsAndCorridors()
    {
        rooms = new Room[numRooms.Random];
        corridors = new Corridor[rooms.Length - 1];

        rooms[0] = new Room();
        corridors[0] = new Corridor();

        rooms[0].SetupRoom(roomWidth, roomHeight, columns, rows);
        corridors[0].SetupCorridor(rooms[0], corridorLength, roomWidth, roomHeight, columns, rows, true);

        // Player spawn
        // Vector3 playerPos = new Vector3(rooms[0].xPos + rooms[0].roomWidth/2, rooms[0].yPos + rooms[0].roomHeight/2, 0);
        // spawnedPlayer = Instantiate(player, playerPos, Quaternion.identity);
        // startingRoom = rooms[0];
        ///////////////

        for (int i = 1; i < rooms.Length; i++)
        {
            rooms[i] = new Room();
            rooms[i].SetupRoom(roomWidth, roomHeight, columns, rows, corridors[i - 1]);

            if (i < corridors.Length)
            {
                corridors[i] = new Corridor();
                corridors[i].SetupCorridor(rooms[i], corridorLength, roomWidth, roomHeight, columns, rows, false);
            }

            // Player spawn
            if(i == 1) //  Mathf.RoundToInt(rooms.Length * 0.5f))
            {
                Vector3 playerPos = new Vector3(rooms[i].xPos + Random.Range(1, rooms[i].roomWidth-1), rooms[i].yPos + Random.Range(1, rooms[i].roomHeight-1), 0);
                spawnedPlayer = Instantiate(player, playerPos, Quaternion.identity);
                startingRoom = rooms[i];
            }
            ///////////////
        }

        Vector3 exitPos = new Vector3(rooms[rooms.Length-1].xPos + rooms[rooms.Length-1].roomWidth/2, rooms[rooms.Length-1].yPos + rooms[rooms.Length-1].roomHeight/2, 0);
        spawnedExit = Instantiate(exit, exitPos, Quaternion.identity);
    }

    void SetTilesValuesForRooms()
    {
        // Items stuff
        int itemsLeftToSpawnInLevel = itemsPerLevel.Random;

        HashSet<int> roomsToSpawnItems = new HashSet<int>();

        for(int i = 0; i < rooms.Length; i++) 
            if(rooms[i] != startingRoom)
                roomsToSpawnItems.Add(i);

        while(roomsToSpawnItems.Count > itemsLeftToSpawnInLevel)
            roomsToSpawnItems.Remove(Random.Range(0, rooms.Length));   
        //////////////

        for (int i = 0; i < rooms.Length; i++)
        {
            Room currentRoom = rooms[i];

            // Monster spawn stuff
            int monstersLeftToSpawn = (int)Mathf.Clamp(monstersPerRoom.Random, monstersPerRoom.m_Min, currentRoom.roomWidth * currentRoom.roomHeight * 0.5f);
            // Trap spawn stuff
            int trapsLeftToSpawn = (int)Mathf.Clamp(trapsPerRoom.Random, trapsPerRoom.m_Min, currentRoom.roomWidth * currentRoom.roomHeight);  
            int[][] shape = ShapeGenerator.GenerateShape(currentRoom.roomWidth, currentRoom.roomHeight, roomHeight.m_Min < roomWidth.m_Min ? roomHeight.m_Min : roomWidth.m_Min);

            // Items stuff
            int randX = Random.Range(0, currentRoom.roomWidth) + currentRoom.xPos;
            int randY = Random.Range(0, currentRoom.roomHeight) + currentRoom.yPos;

            // float roomWeight = (float)(currentRoom.roomWidth * currentRoom.roomHeight) / (float)(roomWidth.m_Max * roomHeight.m_Max);

            // Items spawn stuff
            if(roomsToSpawnItems.Contains(i) && itemsLeftToSpawnInLevel > 0)
            {
                tiles[randX][randY] = TileType.Items;
                itemsLeftToSpawnInLevel--;
            }
            ////////////////////

            for (int x = 0; x < currentRoom.roomWidth; x++)
            {
                int xCoord = currentRoom.xPos + x;

                for (int y = 0; y < currentRoom.roomHeight; y++)
                {
                    int yCoord = currentRoom.yPos + y;

                    if(tiles[xCoord][yCoord] == TileType.Trap || tiles[xCoord][yCoord] == TileType.Items || tiles[xCoord][yCoord] == TileType.Enemy)
                        continue;
                    
                    tiles[xCoord][yCoord] = TileType.Floor;                                    
                    
                    // Traps and monsters spawning stuff
                    if(currentRoom == startingRoom)
                        continue;
                    if(new Vector3(xCoord, yCoord, 0) == spawnedPlayer.transform.position || new Vector3(xCoord, yCoord, 0) == spawnedExit.transform.position)
                        continue;

                    int tilesLeft = (currentRoom.roomWidth - x - 1)*(currentRoom.roomHeight) + (currentRoom.roomHeight - y);

                    // Traps
                    // if(trapsLeftToSpawn > 0 && Random.value <= trapSpawnChance/100f)
                    // {
                    //     tiles[xCoord][yCoord] = TileType.Trap;
                    //     trapsLeftToSpawn--;
                    //     continue;
                    // } 
                    
                    // Monsters
                    if(monstersLeftToSpawn > 0 && (monstersLeftToSpawn == tilesLeft || Random.value <= monsterSpawnChance/100f)) // Random.value <= roomWeight * (monsterSpawnChance/100f))
                    {
                        tiles[xCoord][yCoord] = TileType.Enemy;
                        monstersLeftToSpawn--;
                        continue;
                    }
                    
                    if(trapsLeftToSpawn > 0 && shape[x][y] == 1 && (trapsLeftToSpawn == tilesLeft || Random.value <= trapSpawnChance/100f))
                    {
                        tiles[xCoord][yCoord] = TileType.Trap;
                        trapsLeftToSpawn--;
                        continue;
                    }                         
                    ////////////////////////////////////
                }
            }
        }        
    }

    void SetTilesValuesForCorridors()
    {
        for (int i = 0; i < corridors.Length; i++)
        {
            Corridor currentCorridor = corridors[i];

            for (int x = 0; x < currentCorridor.corridorLength; x++)
            {
                int xCoord = currentCorridor.startXPos;
                int yCoord = currentCorridor.startYPos;

                switch (currentCorridor.direction)
                {
                    case Direction.North:
                        yCoord += x;
                        break;
                    case Direction.East:
                        xCoord += x;
                        break;
                    case Direction.South:
                        yCoord -= x;
                        break;
                    case Direction.West:
                        xCoord -= x;
                        break;
                }

                tiles[xCoord][yCoord] = TileType.Floor;
            }
        }
    }

    void CreateClearPath()
    {
        for(int i = 1; i < rooms.Length; i++)
        {
            Room currentRoom = rooms[i];
            Corridor currentCorridor = corridors[i-1];
            Vector2Int startPos = new Vector2Int(currentRoom.xPos + currentRoom.roomWidth/2, currentRoom.yPos + currentRoom.roomHeight/2);
            Vector2Int endPos = new Vector2Int(currentCorridor.EndPositionX, currentCorridor.EndPositionY);              
            RemoveTrapsOnPath(startPos, endPos);

            if(i < rooms.Length-1)
            {
                currentCorridor = corridors[i];
                endPos = new Vector2Int(currentCorridor.startXPos, currentCorridor.startYPos);
                switch(currentCorridor.direction)
                {
                    case Direction.North:
                        endPos += new Vector2Int(0, -1);
                        break;
                    case Direction.East:
                        endPos += new Vector2Int(-1, 0);
                        break;
                    case Direction.South:
                        endPos += new Vector2Int(0, 1);
                        break;
                    case Direction.West:
                        endPos += new Vector2Int(1, 0);
                        break;
                }         
                RemoveTrapsOnPath(startPos, endPos);
            }
        }
    }

    void RemoveTrapsOnPath(Vector2Int startPos, Vector2Int endPos)
    {
        // int c = 100;
        bool end = false;
        while(!end)
        {
            if(startPos == endPos)
                end = true;

            Vector2Int direction = endPos - startPos;
            if(Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
                direction.y = 0;
            else
                direction.x = 0;
            direction = new Vector2Int(direction.x == 0 ? 0 : direction.x > 0 ? 1 : -1, direction.y == 0 ? 0 : direction.y > 0 ? 1 : -1);

            if(startPos.x > tiles.Length || startPos.y > tiles[0].Length)
                continue;
            
            if(tiles[startPos.x][startPos.y] == TileType.Trap)
            {
                tiles[startPos.x][startPos.y] = TileType.Floor;
                // print("Removing trap at: " + startPos);
            }  

            startPos += direction;

            // c--;
            // if(c <= 0)
            // {
            //     Debug.LogError("INFINITE LOOP!");
            //     break;
            // }
        }
    }

    void InstantiateTiles()
    {
        for (int i = 0; i < tiles.Length; i++)
        {
            for (int j = 0; j < tiles[i].Length; j++)
            {
                InstantiateFromArray(floorTiles, i, j);

                switch(tiles[i][j])
                {
                    case TileType.Items:
                        InstantiateFromArray(items, i, j);
                        break;
                    case TileType.Trap:
                        InstantiateFromArray(trapTiles, i, j);
                        break;
                    case TileType.Enemy:
                        InstantiateFromArray(enemies, i, j);
                        break;
                    case TileType.Wall:
                        InstantiateFromArray(wallTiles, i, j);
                        break;
                }
            }
        }
    }

    void InstantiateOuterWalls()
    {
        float leftEdgeX = -1f;
        float rightEdgeX = columns;
        float bottomEdgeY = -1f;
        float topEdgeY = rows;

        InstantiateVerticalOuterWalls(leftEdgeX, bottomEdgeY, topEdgeY);
        InstantiateVerticalOuterWalls(rightEdgeX, bottomEdgeY, topEdgeY);

        InstantiateHorizontalOuterWalls(leftEdgeX + 1f, rightEdgeX - 1f, bottomEdgeY);
        InstantiateHorizontalOuterWalls(leftEdgeX + 1f, rightEdgeX - 1f, topEdgeY);
    }

    void InstantiateVerticalOuterWalls(float xCoord, float startingY, float endingY)
    {
        float currentY = startingY;
        while (currentY <= endingY)
        {
            InstantiateFromArray(outerWallTiles, xCoord, currentY);
            currentY++;
        }
    }

    void InstantiateHorizontalOuterWalls(float startingX, float endingX, float yCoord)
    {
        float currentX = startingX;
        while (currentX <= endingX)
        {
            InstantiateFromArray(outerWallTiles, currentX, yCoord);
            currentX++;
        }
    }

    void InstantiateFromArray(GameObject[] prefabs, float xCoord, float yCoord)
    {
        int randomIndex = Random.Range(0, prefabs.Length);

        Vector3 position = new Vector3(xCoord, yCoord, 0);
        Instantiate(prefabs[randomIndex], position, Quaternion.identity, boardHolder.transform);
    }

}
