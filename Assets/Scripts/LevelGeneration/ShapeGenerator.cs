﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeGenerator : MonoBehaviour
{
    public enum ShapeType { snake, elipse, circle };

    public static int[][] GenerateShape(int width, int height, int minSize)
    {
        int[][] shape = new int[width][];
        for(int i = 0; i < shape.Length; i++)
        {
            shape[i] = new int[height];
        }

        ShapeType shapeType = (ShapeType)Random.Range(0, (int)ShapeType.circle); // ShapeType.snake;
        bool inner = Random.value > 0.5f;    

        Vector2 center = new Vector2(width/2, height/2);

        switch(shapeType)
        {
            case ShapeType.elipse:
                int xRadius = Random.Range(minSize+1, width-1);
                xRadius *= xRadius;
                int yRadius = Random.Range(minSize+1, height-1);
                yRadius *= yRadius;

                for(int x = 0; x < shape.Length; x++)
                {
                    for(int y = 0; y < shape[x].Length; y++)
                    {                
                        float elipseX = Mathf.Pow(x - center.x, 2) * yRadius;
                        float elipseY = Mathf.Pow(y - center.y, 2) * xRadius;
                        if(elipseX + elipseY <= xRadius * yRadius)
                            shape[x][y] = inner ? 1 : 0;
                        else
                            shape[x][y] = inner ? 0 : 1;
                    }
                }
                break;
            case ShapeType.circle:
                int radius = Random.Range(minSize+1, (width < height ? width : height) - 2);

                for(int x = 0; x < shape.Length; x++)
                {
                    for(int y = 0; y < shape[x].Length; y++)
                    {
                        Vector2 cur = new Vector2(x, y);
                        if(Vector2.Distance(cur, center) < radius)
                            shape[x][y] = inner ? 1 : 0;
                        else
                            shape[x][y] = inner ? 0 : 1;
                    }
                }
                break;
            case ShapeType.snake:
                bool top = true;
                for(int x = 0; x < shape.Length; x++)
                {
                    for(int y = 0; y < shape[x].Length; y++)
                    {
                        shape[x][y] = inner ? 0 : 1;

                        if(top && y == 0 || !top && y == shape[x].Length-1)
                            shape[x][y] = inner ? 1 : 0;
                        if(x % 2 == 0)
                            continue;
                        
                        shape[x][y] = inner ? 1 : 0;
                    }  
                    if(x % 2 == 0)
                        top = !top;                  
                }
                break;
        }        

        return shape;
    }

    // public static void PrintShape(int[][] shape)
    // {
    //     string msg = "";
    //     for(int x = 0; x < shape.Length; x++)
    //     {            
    //         for(int y = 0; y < shape[x].Length; y++)
    //         {
    //             msg += "[" + shape[x][y] + "]";
    //         }
    //         msg += "\n";
    //     }    
    //     print(msg);    
    // }
}
