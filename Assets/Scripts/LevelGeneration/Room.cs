﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room
{
    public int xPos;
    public int yPos;
    public int roomWidth;
    public int roomHeight;
    public Direction enteringCorridor;

    public void SetupRoom(IntRange widthRange, IntRange heightRange, int columns, int rows)
    {
        roomWidth = widthRange.Random;
        roomHeight = heightRange.Random;

        xPos = Mathf.RoundToInt(columns / 2f - roomWidth / 2f);
        yPos = Mathf.RoundToInt(rows / 2f - roomHeight / 2f);
    }

    public void SetupRoom(IntRange widthRange, IntRange heightRange, int columns, int rows, Corridor corridor)
    {
        enteringCorridor = corridor.direction;

        roomWidth = widthRange.Random;
        roomHeight = heightRange.Random;

        switch (corridor.direction)
        {
            case Direction.North:
            {
                roomHeight = Mathf.Clamp(roomHeight, 1, rows - corridor.EndPositionY);
                yPos = corridor.EndPositionY;
                xPos = Random.Range(corridor.EndPositionX - roomWidth + 1, corridor.EndPositionX);
                xPos = Mathf.Clamp(xPos, 0, columns - roomWidth);
                break;
            }
            case Direction.East:
            {
                roomWidth = Mathf.Clamp(roomWidth, 1, columns - corridor.EndPositionX);
                xPos = corridor.EndPositionX;
                yPos = Random.Range(corridor.EndPositionY - roomHeight + 1, corridor.EndPositionY);
                yPos = Mathf.Clamp(yPos, 0, rows - roomHeight);
                break;
            }
            case Direction.South:
            {
                roomHeight = Mathf.Clamp(roomHeight, 1, corridor.EndPositionY);
                yPos = corridor.EndPositionY - roomHeight + 1;
                xPos = Random.Range(corridor.EndPositionX - roomWidth + 1, corridor.EndPositionX);
                xPos = Mathf.Clamp(xPos, 0, columns - roomWidth);
                break;
            }
            case Direction.West:
            {
                roomWidth = Mathf.Clamp(roomWidth, 1, corridor.EndPositionX);
                xPos = corridor.EndPositionX - roomWidth + 1;
                yPos = Random.Range(corridor.EndPositionY - roomHeight + 1, corridor.EndPositionY);
                yPos = Mathf.Clamp(yPos, 0, rows - roomHeight);
                break;
            }
        }
    }

    // // [System.Serializable]
    // // public class Tile
    // // {
    // //     public int number;
    // //     public GameObject prefab;
    // // }

    // // public Tile[] tiles;

    // public int maxWidth = 10;
    // public int minWidth = 5;

    // public int maxHeight = 10;
    // public int minHeight = 5;

    // public int randomizedWidth;
    // public int randomizedHeight;    

    // public int chanceForIrregularTiles = 50;
    // public int minNeighboursForIrregularTile = 4;
    // public int minNeighboursToFillTile = 7;

    // public int[,] room;

    // void Awake() 
    // {
    //     CreateRoom();
    // }

    // void CreateRoom()
    // {
    //     int height = Random.Range(minHeight, maxHeight+1);
    //     int width = Random.Range(minWidth, maxWidth+1);
    //     randomizedHeight = height;
    //     randomizedWidth = width;

    //     room = new int[width,height];

    //     for(int x = 0; x < width; x++)
    //     {
    //         for(int y = 0; y < height; y++)
    //         {
    //             if(x == 0 || y == 0 || x == width-1 || y == height-1)
    //                 room[x,y] = 1;
    //             else
    //                 room[x,y] = 0;
    //         }
    //     }

    //     for(int x = 1; x < width-1; x++)
    //     {
    //         for(int y = 1; y < height-1; y++)
    //         {
    //             if(CountNeighbours(x,y) >= minNeighboursForIrregularTile && Random.value < chanceForIrregularTiles/100f)
    //             {
    //                 room[x,y] = 1;
    //             }
    //         }
    //     }

    //     for(int x = 1; x < width-1; x++)
    //     {
    //         for(int y = 1; y < height-1; y++)
    //         {
    //             if(CountNeighbours(x,y) >= minNeighboursToFillTile)
    //             {
    //                 room[x,y] = 1;
    //             }
    //         }
    //     }

    //     BoxCollider2D collider = GetComponent<BoxCollider2D>();
    //     collider.size = new Vector2(randomizedWidth, randomizedHeight);
    //     collider.offset = new Vector2((randomizedWidth-1)/2f, (randomizedHeight-1)/2f);
    // }

    // int CountNeighbours(int tileX, int tileY)
    // {
    //     int count = 0;

    //     for(int x = -1; x <= 1; x++)
    //     {
    //         for(int y = -1; y <= 1; y++)
    //         {
    //             if(x == 0 && y == 0)
    //                 continue;

    //             if(x+tileX < 0 || x+tileX >= randomizedWidth || y+tileY < 0 || y+tileY >= randomizedHeight)
    //                 continue;

    //             if(room[x+tileX,y+tileY] == 1)
    //                 count++;
    //         }
    //     }

    //     return count;
    // }

    // // public List<GameObject> SpawnTiles()
    // // {
    // //     List<GameObject> spawnedTiles = new List<GameObject>();

    // //     for(int x = 0; x < randomizedWidth; x++)
    // //     {
    // //         for(int y = 0; y < randomizedHeight; y++)
    // //         {
    // //             for(int i = 0; i < tiles.Length; i++)
    // //             {
    // //                 if(room[x,y] == tiles[i].number)
    // //                 {
    // //                     spawnedTiles.Add(Instantiate(tiles[i].prefab, transform.position + new Vector3(x,y,0), Quaternion.identity, transform));
    // //                 }
    // //             }
    // //         }
    // //     }

    // //     return spawnedTiles;
    // // }

    // // void OnDrawGizmos() 
    // // {
    // //     if(room != null)
    // //     {
    // //         for(int x = 0; x < randomizedWidth; x++)
    // //         {
    // //             for(int y = 0; y < randomizedHeight; y++)
    // //             {
    // //                 Gizmos.color = room[x,y] == 0 ? Color.white : Color.cyan;
    // //                 // Gizmos.color = room[x,y] == 2 ? Color.red : Gizmos.color;
    // //                 Gizmos.DrawCube(new Vector3(x,y,0) + transform.position, Vector3.one * 1f);
    // //             }
    // //         }
    // //     }
    // // }    
}
