﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// public enum TileType { filler, wall, floor };  

// [System.Serializable]
// public class Tile
// {     
//     public TileType typeOfTile;
//     public GameObject prefab;
// }

public class Level : MonoBehaviour
{
    // public int minNumberOfRooms = 8;
    // public int maxNumberOfRooms = 10;

    // public int chanceToChangeDirection = 50;

    // Room[] level;
    // TileType[,] levelLayout;
    // int levelWidth;
    // int levelHeight;

    // public GameObject room;
    // public Tile[] tiles;

    // public enum Direction { North, East, South, West };

    // void Update()
    // {
    //     if(Input.GetKeyDown(KeyCode.R))
    //     {
    //         foreach(Transform child in transform)
    //         {
    //             Destroy(child.gameObject);
    //         }
    //         CreateLevel();
    //     }
    // }

    // void CreateLevel()
    // {
    //     int numberOfRooms = Random.Range(minNumberOfRooms, maxNumberOfRooms+1);

    //     level = new Room[numberOfRooms];

    //     Vector3 roomPosition = Vector3.zero;
    //     Direction direction = Direction.North;        
    //     level[0] = Instantiate(room, roomPosition, Quaternion.identity, transform).GetComponent<Room>();

    //     for(int i = 1; i < numberOfRooms; i++)
    //     {
    //         Vector3 oldPosition = roomPosition;
            
    //         Room spawnedRoom = Instantiate(room, transform).GetComponent<Room>();            
    //         level[i] = spawnedRoom;

    //         SwitchPosition(ref roomPosition, direction, level[i-1].maxWidth, level[i-1].maxHeight);
    //         spawnedRoom.GetComponent<BoxCollider2D>().enabled = false;

    //         int x = 0;
    //         while(OccupiedByRoom(roomPosition, level[i].maxWidth, level[i].maxHeight))
    //         {
    //             roomPosition = oldPosition;
    //             ChangeDirection(ref direction);
    //             SwitchPosition(ref roomPosition, direction, level[i-1].maxWidth, level[i-1].maxHeight);  
                
    //             x++;
    //             if(x > (int)Direction.West+1)
    //             {
    //                 // Debug.Log("No empty direction!");
    //                 roomPosition = level[Random.Range(0, i)].transform.position;
    //                 oldPosition = roomPosition;
    //                 x = 0;
    //             }
    //         }

    //         spawnedRoom.transform.position = roomPosition;            
    //         spawnedRoom.GetComponent<BoxCollider2D>().enabled = true;

    //         if(Random.value < chanceToChangeDirection/100f)
    //             ChangeDirection(ref direction);
    //     }

    //     Vector3 bottomLeftCorner = Vector3.zero;
    //     Vector3 topRightCorner = Vector3.zero;
    //     for(int i = 0; i < numberOfRooms; i++)
    //     {
    //         if(level[i].transform.position.x < bottomLeftCorner.x)
    //         {
    //             bottomLeftCorner = new Vector3(level[i].transform.position.x, bottomLeftCorner.y, 0);
    //         }
    //         if(level[i].transform.position.y < bottomLeftCorner.y)
    //         {
    //             bottomLeftCorner = new Vector3(bottomLeftCorner.x, level[i].transform.position.y, 0);
    //         }

    //         if(level[i].transform.position.x > topRightCorner.x)
    //         {
    //             topRightCorner = new Vector3(level[i].transform.position.x, topRightCorner.y, 0);
    //         }
    //         if(level[i].transform.position.y > topRightCorner.y)
    //         {
    //             topRightCorner = new Vector3(topRightCorner.x, level[i].transform.position.y, 0);
    //         }           
    //     }
    //     topRightCorner += new Vector3(level[0].maxWidth, level[0].maxHeight, 0);

    //     int width = (int)(topRightCorner.x - bottomLeftCorner.x);
    //     int height = (int)(topRightCorner.y - bottomLeftCorner.y);        

    //     levelWidth = 2*(width+1);
    //     levelHeight = 2*(height+1);
    //     levelLayout = new TileType[levelWidth, levelHeight];

    //     for(int i = 0; i < numberOfRooms; i++)
    //     {
    //         for(int x = 0; x < level[i].randomizedWidth; x++)
    //         {
    //             for(int y = 0; y < level[i].randomizedHeight; y++)
    //             {
    //                 Vector3 pos = new Vector3(x, y, 0) + level[i].transform.position;
    //                 if(level[i].room[x,y] == 0)
    //                     levelLayout[(int)pos.x+levelWidth/2, (int)pos.y+levelHeight/2] = TileType.floor;
    //                 else
    //                     levelLayout[(int)pos.x+levelWidth/2, (int)pos.y+levelHeight/2] = TileType.wall;
    //             }
    //         }
    //     }

    //     for(int i = 1; i < numberOfRooms; i++)
    //     {
    //         PathFromRoomToRoom(ref level[i-1], ref level[i]);
    //     }

    //     SpawnTiles();
    // }

    // void SpawnTiles()
    // {
    //     if(levelLayout != null)
    //     {
    //         for(int x = 0; x < levelWidth; x++)
    //         {
    //             for(int y = 0; y < levelHeight; y++)
    //             {
    //                 for(int i = 0; i < tiles.Length; i++)
    //                 {
    //                     if(levelLayout[x,y] == tiles[i].typeOfTile)
    //                     {
    //                         Vector3 pos = new Vector3(x-levelWidth/2f, y-levelHeight/2f, 0);
    //                         Instantiate(tiles[i].prefab, pos, Quaternion.identity, transform);
    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }

    // // void OnDrawGizmos() 
    // // {
    // //     if(levelLayout != null)
    // //     {
    // //         for(int x = 0; x < levelWidth; x++)
    // //         {
    // //             for(int y = 0; y < levelHeight; y++)
    // //             {
    // //                 if(levelLayout[x,y] == TileType.filler)
    // //                 {
    // //                     Gizmos.color = Color.cyan;
    // //                 }
    // //                 else if (levelLayout[x,y] == TileType.wall)
    // //                 {
    // //                     Gizmos.color = Color.white;
    // //                 }
    // //                 else if (levelLayout[x,y] == TileType.floor)
    // //                 {
    // //                     Gizmos.color = Color.red;
    // //                 }
    // //                 Gizmos.DrawCube(new Vector3(x-levelWidth/2f, y-levelHeight/2f, 0), Vector3.one * 1f);
    // //             }
    // //         }
    // //     }
    // // }

    // void PathFromRoomToRoom(ref Room roomA, ref Room roomB)
    // {
    //     Vector3 roomACenter = roomA.transform.position + new Vector3((roomA.randomizedWidth-1)/2, (roomA.randomizedHeight-1)/2, 0);
    //     Vector3 roomBCenter = roomB.transform.position + new Vector3((roomB.randomizedWidth-1)/2, (roomB.randomizedHeight-1)/2, 0);
        
    //     Vector3 currentPos = roomACenter;

    //     int c = 1000;
    //     while(Vector3.Distance(currentPos, roomBCenter) > Mathf.Epsilon)
    //     {
    //         Vector3 direction = roomBCenter - currentPos;

    //         if(Mathf.Abs(direction.x) > 0)
    //         {
    //             direction.x = direction.x > 0 ? 1 : -1;
    //             direction.y = 0;
    //         }
    //         else
    //         {
    //             direction.y = direction.y > 0 ? 1 : -1;
    //             direction.x = 0;
    //         }
            
    //         for(int i = 0; i < level.Length; i++)
    //         {
    //             Room _room = level[i];
    //             int x = (int)(currentPos.x - _room.transform.position.x);
    //             int y = (int)(currentPos.y - _room.transform.position.y);

    //             if(x < _room.randomizedWidth && x >= 0 && y < _room.randomizedHeight && y >= 0)
    //             {
    //                 _room.room[x,y] = 2;
    //             }
    //         }

    //         // int x = (int)(currentPos.x - roomA.transform.position.x);
    //         // int y = (int)(currentPos.y - roomA.transform.position.y);

    //         // if(x < roomA.randomizedWidth && x >= 0 && y < roomA.randomizedHeight && y >= 0)
    //         //     roomA.room[x,y] = 2;
            
    //         // x = (int)(currentPos.x - roomB.transform.position.x);
    //         // y = (int)(currentPos.y - roomB.transform.position.y);

    //         // if(x < roomB.randomizedWidth && x >= 0 && y < roomB.randomizedHeight && y >= 0)
    //         //     roomB.room[x,y] = 2;                       

    //         levelLayout[(int)currentPos.x+levelWidth/2, (int)currentPos.y+levelHeight/2] = TileType.floor;
    //         currentPos += direction;

    //         c--;
    //         if(c <= 0)
    //             break;
    //     }
    // }

    // void ChangeDirection(ref Direction direction)
    // {
    //     if(direction < Direction.West)
    //         direction++;
    //     else
    //         direction = Direction.North;
    // }

    // void SwitchPosition(ref Vector3 roomPosition, Direction direction, int x, int y)
    // {
    //     switch(direction)
    //     {
    //         case Direction.North:
    //         {
    //             roomPosition += new Vector3(0, y, 0);
    //             break;
    //         }
    //         case Direction.East:
    //         {
    //             roomPosition += new Vector3(x, 0, 0);
    //             break;
    //         }
    //         case Direction.South:
    //         {
    //             roomPosition += new Vector3(0, -y, 0);
    //             break;
    //         }
    //         case Direction.West:
    //         {
    //             roomPosition += new Vector3(-x, 0, 0);
    //             break;
    //         }
    //     }
    // }

    // bool OccupiedByRoom(Vector3 position, int width, int height)
    // {
    //     Vector3 size = new Vector3((width-1)/2f, (height-1)/2f, 0);
    //     position += size;
    //     RaycastHit2D hit = Physics2D.BoxCast(position, size, 0, Vector2.up, 0);
        
    //     // Debug.DrawLine(position, position+size, Color.red, 2f);
        
    //     return hit;

    //     // for(int i = 0; i < level.Length; i++)
    //     // {
    //     //     if(level[i] == null)
    //     //         continue;
            
    //     //     for(int x = 0; x < width; x++)
    //     //     {
    //     //         for(int y = 0; y < height; y++)
    //     //         {
    //     //             if(position.x+x >= level[i].transform.position.x && position.x+x <= level[i].transform.position.x+level[i].randomizedWidth
    //     //             && position.y+y >= level[i].transform.position.y && position.y+y <= level[i].transform.position.y+level[i].randomizedHeight)
    //     //                 return true;
    //     //         }
    //     //     }
    //     // }
    //     // return false;
    // }
}
