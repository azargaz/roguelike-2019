﻿using System;

[Serializable]
public class IntRange
{
    public int m_Min;
    public int m_Max;

    public IntRange(int a, int b)
    {
        m_Min = a;
        m_Max = a;
    }

    public int Random
    {
        get { return UnityEngine.Random.Range(m_Min, m_Max); }
    }
}
