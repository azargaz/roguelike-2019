﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnManager : MonoBehaviour
{
    public static TurnManager instance;
    public Pathfinding pathfinding;
    
    public List<Enemy> enemies;
    Dictionary<Vector3, int> plannedMovements;

    [HideInInspector]
    public bool playersTurn = true;
    bool enemiesMoving = false;
    
    public float turnDelay = 0.001f;
    float waitForEnemyTurn;

    Player player;

    void Awake()
    {
        instance = this;
        enemies = new List<Enemy>();
        plannedMovements = new Dictionary<Vector3, int>();
        player = GameObject.FindGameObjectWithTag ("Player").GetComponent<Player>();
        waitForEnemyTurn = player.moveTime + turnDelay;
    }

    void Update()
    {
        if(playersTurn || enemiesMoving)
            return;

        if(player == null)
            player = GameObject.FindGameObjectWithTag ("Player").GetComponent<Player>();
        
        StartCoroutine(MoveEnemies());
    }
    
    public void StartPlayersTurn()
    {
        playersTurn = true;
    }

    public void EndPlayersTurn()
    {
        playersTurn = false;
    }

    public void AddEnemy(Enemy enemy)
    {
        enemies.Add(enemy);
        EnemiesUpdated();
    }    

    public void RemoveEnemy(Enemy enemy)
    {
        enemies.Remove(enemy);
        EnemiesUpdated();
    }

    void EnemiesUpdated()
    {
        pathfinding.UpdateEnemyPositions();
    }
    
    IEnumerator MoveEnemies()
    {
        enemiesMoving = true;

        // Wait until player finishes movement
        yield return new WaitForSeconds(waitForEnemyTurn);
        
        // Dictionary of planned movements, needed to count if more than 1 enemy plans to move to certain tile
        plannedMovements.Clear();
        // Array to store planned movements
        Vector3[] plannedMoves = new Vector3[enemies.Count];
        int enemiesMoved = 0;

        // Add planned moves to dictionary and count them
        for (int i = 0; i < enemies.Count; i++)
        {
            plannedMoves[i] = enemies[i].PlanMoveEnemy() + enemies[i].transform.position;
            
            if(plannedMovements.ContainsKey(plannedMoves[i]))
            {
                plannedMovements[plannedMoves[i]]++;
            }
            else
            {
                plannedMovements.Add(plannedMoves[i], 1);
            }                    
        }
        // Check if any move is already planned, unless it is player's position, then just move to that field attacking player
        for (int i = 0; i < enemies.Count; i++)
        {    
            if(!enemies[i].aggroed)
                continue;
            
            if(enemies[i].target.position == plannedMoves[i])
            {
                enemies[i].MoveEnemy();
                enemiesMoved++;
            }
            else
            {
                if(plannedMovements[plannedMoves[i]] > 1)
                {
                    plannedMovements[plannedMoves[i]]--;
                }
                else
                {
                    enemies[i].MoveEnemy();
                    enemiesMoved++;
                }
            }
        }

        // Wait for enemies to finish their movements
        if(enemiesMoved > 0)
            yield return new WaitForSeconds(enemies[0].moveTime);
        
        StartPlayersTurn();
        enemiesMoving = false;
    }
}